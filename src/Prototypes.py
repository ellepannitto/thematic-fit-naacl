'''
 
 Prototypes.py - Contains the function that builds a dictionary of prototype vectors
 
   
  Copyright (C) 2018  Enrico Santus, Ludovica Pannitto, Emmanuele Chersoni
  Ludovica.pannitto@unitn.it
    
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
 
'''


import collections
import numpy as np
import logging

logger = logging.getLogger('thematic_fit_calc')

def create_proto(fillers, model, n):
	'''
	 Creates an instance of IndexedSpace.
		 
	 :param: fillers dictionary of target-relation pairs for which a centroid is needed. For each target, for each relation, the dictionary contains the list of fillers that are summed into the centroid.
	 :param: model distributional model
	 :param: n number of fillers to consider for the centroid vector
	 
	 :returns: d dictionary containing for each target, for each relation, the prototype vector
		 
	'''
	
	d = collections.defaultdict(dict)
	
	ntarget = 0
	for target in fillers:
		
		logger.info ("processing word: {}, processed {} out of {}".format(target, ntarget, len(fillers)))
		ntarget+=1
		
		for rel in fillers[target]:
			
			if len(fillers[target][rel])<n:
				logger.warning("not enough fillers for {}+{} ({})".format(target, rel, len(fillers[target][rel])))
			
			considered_fillers = fillers[target][rel][:n]
			
			temporary_centroid = np.zeros(model.ncols)
			
			nprocessed = 0
			for x, y in considered_fillers:
				
				logger.info ("processed {} out of {}".format(nprocessed, len(considered_fillers)))
				
				try:
					temporary_centroid = np.sum([temporary_centroid, model[x]], axis = 0)					
				except:
					logger.warning("skipping filler {} for target {}: the filler is not in the dsm".format(x, target))
					
				nprocessed +=1

			
			if not rel=="all":
				d[target][rel] = temporary_centroid
			else:
				d[target]["sbj"] = temporary_centroid
				d[target]["obj"] = temporary_centroid
				d[target]["loc"] = temporary_centroid
				d[target]["with"] = temporary_centroid
				
	
	return d
