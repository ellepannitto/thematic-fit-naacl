'''
 
 SemanticSpace.py - Contains classes that represent semantic spaces with interfaces for loading, querying and dumping the spaces.
  
  
  Copyright (C) 2018  Enrico Santus, Ludovica Pannitto, Emmanuele Chersoni
  Ludovica.pannitto@unitn.it
    
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

'''

import gzip
import numpy as np
import collections
from scipy import sparse
from scipy.stats import uniform
import indexed_gzip as igzip
import os

import logging

logger = logging.getLogger('thematic_fit_calc')

class IndexedSpace (collections.MutableMapping):
	'''
	 Class that represent a semantic space, to be used when the space is so big that it does not fit in memory.
	 
	 Most of the information needed to represent the space is stored in a file on the disk using the compressed gzip format while a tiny index is kept in memory.
	 The index allows to read the file in a "random access" fashion to save time when querying the space. 
	'''
	
	def __init__ (self, fname, to_load, buffer_size = 1024*1024):
		'''
		 Creates an instance of IndexedSpace.
		 
		 :param: fname name of the file containing the semantic space. It must exist and be a valid gzip file.
		 :param: to_load set. Only the targets (i.e., rows) in to_load will be included in the space. If it is None, all the targets are loaded
		 :param: buffer_size parameter used when building the index. Bigger values will result in smaller memory usage, but slower query time. 
		               on the other hand, smaller values will result in bigger memory usage but faster query time. If unsure, use the default value (1024*1024)
		 
		'''
		
		self.buffer_size = buffer_size
		self.fname = fname
		self.to_load = to_load
		
		self.nrows = 0
		self.ncols = 0
		self.cols = {}
		self.last_col = 0
		
		self.fout, to_build = self.create_fout()
		
		if to_build:
		
			self.index_lines = []
			self.index_offsets = []			
			self.rebuild_index (to_load)
			self.dump_info()
		else:	
			self.index_lines, self.index_offsets, self.cols, self.ncols = self.load_info()
			#~ self.dump_info()
			self.file_handler = igzip.IndexedGzipFile (self.fout_name, spacing=self.buffer_size)
		
	def dump_info (self):
		'''
		 Dumps the index on a file such that it can be later reloaded using the function load_info.
		 It is not possible to choose the file on which the dumping is done: it is automatically determined and the same file is used by load_info to load the index when needed.
		'''
		
		self.fout_index_lines = open("../tmp/index_lines_{}".format(self.fname.rsplit("/", 1)[-1]), "w")
		self.fout_index_offsets = open("../tmp/index_offsets_{}".format(self.fname.rsplit("/", 1)[-1]), "w")
		
		self.fout_cols = open("../tmp/index_cols_{}".format(self.fname.rsplit("/", 1)[-1]), "w")
		
		for line in self.index_lines:
			self.fout_index_lines.write(line)

		for off in self.index_offsets:
			self.fout_index_offsets.write("{}\n".format(off))
		
		for w in self.cols:
			self.fout_cols.write("{} {}\n".format(w, self.cols[w]))
		
		

	def load_info (self):
		'''
		 Loads the index from a file. This can be done to save time instead of rebuilding it from scratch.
		 It is not possible to choose the file from which the index is read: it is automatically determined as the same file used by dump_info to dump the index.
		
		 :returns: lines: list with the index keys
		           offsets: list with the index values i.e. offsets[i] the position of lines[i] in the semantic space files
		           cols: dictionary mapping column labels to indexes
		           maxk: number of columns of the matrix
		 
		'''
		self.fin_index_lines = open("../tmp/index_lines_{}".format(self.fname.rsplit("/", 1)[-1]))
		self.fin_index_offsets = open("../tmp/index_offsets_{}".format(self.fname.rsplit("/", 1)[-1]))
		self.fin_cols = open("../tmp/index_cols_{}".format(self.fname.rsplit("/", 1)[-1]))
		
		index_lines = []
		index_offsets = []
		
		last_checkpoint = -1
		cur_pos = 0
		
		logger.info ("creating semantic space index...")
		
		with gzip.open ( self.fout_name, "rt", errors="ignore" ) as fin:
			
			line = fin.readline()
			
			while line:
					
				#time for a checkpoint
				if last_checkpoint<0 or last_checkpoint + self.buffer_size <= cur_pos:
					index_lines.append (line)
					index_offsets.append (cur_pos)
					logger.debug ("added a checkpoint to the index: LINE {} OFFSET {}".format(line.strip(), cur_pos))
					last_checkpoint = cur_pos
				cur_pos = fin.tell()
				line = fin.readline()
		
		logger.info ("Created semantic space index.")

		#~ index_lines = self.fin_index_lines.readlines()
		#~ index_offsets = [int(line.strip()) for line in self.fin_index_offsets.readlines()]
		
		cols = {}
		max_k = 0
		for line in self.fin_cols:
			v, k = line.strip().split()
			k = int(k)
			cols[v] = k
			if k > max_k:
				max_k = k
		
		return index_lines, index_offsets, cols, max_k+1	
		
	def create_fout (self):
		'''
		  Check if a dump of the index exists in ../tmp
		  For more information about dumping and loading the index, see the dump_info() and load_info() documentation
		  
		  :returns: fin,False if there is a dump available. fin is a file handler from which the dump can be read.
		            fout,True if there are no dumps available. fout is a file handler into which the dump must be written.
		'''
		
		if not os.path.isdir ("../tmp/"):
			os.makedirs ("../tmp/")
		
		self.fout_name = "../tmp/indexed_{}".format(self.fname.rsplit("/", 1)[-1])
		if os.path.exists(self.fout_name):
			return gzip.open(self.fout_name, "rt"), False
		else:
			return gzip.open(self.fout_name, "wt"), True
		
	def rebuild_index (self, to_load):
		'''
		 rebuild the index from scratch, first loading the semantinc space and then indexing it.
		 Only the target in the parameter to_load are loaded from self.fname: the other words are discarded.
		 
		 :param: to_load set that contains the targets to be loaded in the semantic space
		 
		'''
		
		with gzip.open ( self.fname, "rt", errors="ignore" ) as fin:
			
			for line in fin:
				
				w1, w2, f = line.strip().split()
				w1 = w1[:-2]+"-"+w1[-1].lower()
				w2 = w2[:-2]+"-"+w2[-1].lower()
				
				if self.to_load is None or w1 in self.to_load:
					written_line = "{}\n".format(" ".join([w1, w2, f])) 
					self.fout.write( written_line )
					#~ print(w1)
					#~ input()
					if not w2 in self.cols:
						self.cols[w2] = self.last_col
						#~ print(self.cols[w2])
						self.last_col +=1
		self.fout.close()
		
		last_checkpoint = -1
		cur_pos = 0
		
		with gzip.open ( self.fout_name, "rt", errors="ignore" ) as fin:
			
			for line in fin:					
				#time for a checkpoint
				if last_checkpoint<0 or last_checkpoint + self.buffer_size <= cur_pos:
					self.index_lines.append (line)
					self.index_offsets.append (cur_pos)
					logger.debug ("added checkpoint LINE {} OFFSET {}".format(line.strip(), cur_pos))
					last_checkpoint = cur_pos
				
				cur_pos = cur_pos + len(line)				

		self.file_handler = igzip.IndexedGzipFile (self.fout_name, spacing=self.buffer_size)	
		self.ncols = self.last_col

	def get_lines_stating_with ( self, pattern ):
		'''
		 
		 Searches the file that stores the semantic space to retrieve all the lines that concern a pattern specified as a parameter.
		 In particular, if TARGET is given as pattern this function retrieves all the fillers for TARGET, each one with their association score.
		 
		 For example, if pattern is "Boy-"n", this function may returns lines including:
		 
		 Boy-n compound_achievement-n 4.222564550248649
 		 Boy-n compound_act-n 9.4664640790593
 		 Boy-n compound_advance-n 2.486299463520769
		 Boy-n compound_adventure-n 1.6483949754530975
		 Boy-n compound_afro-n 8.172099483265661
		 Boy-n compound_agency-n 2.4900625465098756
		 Boy-n compound_aggregator-n 4.344511382224809
		 ...
		 
		 and so on. All the other lines starting with "Boy-n" are retreived.
		 This function returns an iterator on the retrieved lines, thus it can be used in the following way:
		 
			is = IndexedSpace ( "/path/to/semantic/space.gz", to_load=None )
			for line in is.get_lines_starting_with ("Boy-n"):
				do_something_with (line)
		 
		 :param: pattern the pattern to search for. It can be any string, but it is particularly useful to specify a targer (e.g. "Boy-n") or a target/rel pair (e.g. "Boy-n compound").
		 
		 :yields: all the lines in the space starting with pattern
		  
		'''
		
		logger.debug("searching for {}".format(pattern))
		
		i=0
		
		logger.debug ("searching the index...")

		# TODO: maybe binary search or trie is better
		while i<len(self.index_lines) and self.index_lines[i]<pattern:
			logger.debug ("skipping line in index {}".format(self.index_lines[i]))
			i = i+1
		
		if i>0:
			i = i-1
		logger.debug ("starting reading file from line {}".format(self.index_lines[i]))
		logger.debug ("which is at offset {}".format(self.index_offsets[i]))

		starting_offset_into_uncompressed_data = self.index_offsets[i]
		
		self.file_handler.seek ( starting_offset_into_uncompressed_data )
		
		line = ""
		while not line.startswith ( pattern ) :
			line = self.file_handler.readline ().decode()
			
			logger.debug ("read line {}".format(line))
			if line == "":
				logger.debug ("EOF reached: no lines returned")
				return
		
			if not i >= len(self.index_lines)-2 and line > self.index_lines[i+2]:
				logger.debug ("reached a line > pattern, no lines returned")
				return
		
		logger.debug ("Found first line: yielding {}".format(line))
		yield line
		line = self.file_handler.readline ().decode()

		while line.startswith ( pattern ):
			logger.debug ("found another line: yielding {}".format (line))
			yield line
			line = self.file_handler.readline ().decode()		
	
	def __getitem__(self, key):
		'''
		 returns a vector representing a word in the semantic space.
		 
		 :param: key string: the queried word
		 
		 :returns: np.array of floats that represents key in this semantic space

		'''
		
		explicit_vec = np.zeros(len(self.cols))
		
		lines = self.get_lines_stating_with(key)
		
		indexes = []
		values = []
		
		for l in lines:	
			w1, w2, f = l.strip().split()
			
			indexes.append(self.cols[w2])
			values.append(float(f))

		if len(indexes)>0:
			np.put(explicit_vec, indexes, values)
		else:
			raise KeyError
		
		return explicit_vec
		
	def __delitem__ (self, item):
		pass
	
	def __iter__(self):
		return iter(dict())

	def __len__(self):
		return 0
		
	def __setitem__ (self, item):
		pass



class DenseSpace(collections.MutableMapping):
	'''
	 
	 Class that represent a semantic space as a dense matrix.
	 All the informations needed to represent and query the space are kept in memory.
	    
	'''
	
	def __init__ (self):
		'''
		 Initialize an empty semantic space
		'''
		
		self.nrows = 0
		self.ncols = 0
		
		self.data = {}
	
	def __getitem__(self, key):
		'''
		 returns a vector representing a word in the semantic space.
		 
		 :param: key string: the queried word
		 
		 :returns: np.array of floats that represents key in this semantic space
		'''
		return self.data[key]

	def __setitem__(self, key, value):
		'''
		 Sets the value for a word in a space, discardin the old value if any
		 
		 :param: key string word to which assign the new value
		 :param: value new value
		 
		'''
		self.data[key] = value
		self.nrows += 1
		self.ncols  = np.prod(value.shape)

	def __delitem__(self, key):
		pass

	def __iter__(self):
		return iter(self.data)

	def __len__(self):
		return len(self.data)
		


class SparseSpace(collections.MutableMapping):
	'''
	 Class that represent a semantic space in which most pairs (TARGET-FILLER) have score zero.
	 All the informations needed to represent and query the space are kept in memory: if the space is too big use IndexedSpace instead.
	   
	'''
	
	def __init__(self, row_ind, col_ind, data):
		'''
		 Creates an instance of SparseSpace.
		 
		 :param: row_ind list of row indexes
		 :param: col_ind list of column indexes
		 :param: data list of non-zero values, corresponding to row and column indexed
		 
		'''
		
		self.row_ind = row_ind
		self.col_ind = col_ind
		self.data = data
		self.stored_items = None
		
		self.nrows = 0
		self.ncols = 0
		
		self.fakestore = dict()
		
		self.key_to_index = {}
		self.index_to_key = {}
		self.last_index = 0
		
		self.create_matrix()
		
	def create_matrix(self):
		
		self.row_ind = [self.get_index_from_key(x) for x in self.row_ind]
		self.col_ind = [self.get_index_from_key(x) for x in self.col_ind]
		
		self.nrows = len(set(self.row_ind))
		self.ncols = self.last_index
		
		self.stored_items = sparse.csr_matrix((self.data, (self.row_ind, self.col_ind)))
		
		self.row_ind, self.col_ind, self.data = None, None, None
		
		
	def add_data (self, row, col, value):
		
		self.row_ind.append(row)
		self.col_ind.append(col)
		self.data.append(value)

	def __getitem__(self, key):
		
		i = self.get_index_from_key(key)
		try:
			row = np.array(self.stored_items.getrow(i).todense())[0]
			return row
		except Exception as e:
			raise KeyError

	def __setitem__(self, key, value):
		pass

	def __delitem__(self, key):
		pass

	def __iter__(self):
		return iter(self.fakestore)

	def __len__(self):
		return len(self.fakestore)

	def get_index_from_key (self, key):
		if not key in self.key_to_index:
			self.key_to_index[key] = self.last_index
			self.last_index+=1
		return self.key_to_index[key]
		
	def get_key_from_index (self, i):
		return self.index_to_key[i]
		
	def __str__(self):
		return "KEYS MAPPING: {}\n MATRIX: \n{}".format(self.key_to_index, self.stored_items)

def load_dense (filename, to_load):
	'''
	  Load a dense semantic space from a file.
	  
	  :param: filename the file from which the space has to be loaded. Must be a valid gzip file
	  :param: to_load set. Only the target in to_load will be included in the space. If it is None, all the targets are loaded
	  
	  :returns: DenseSpace instance representing the space
	'''
	
	space = DenseSpace()

	with gzip.open(filename, "rt", errors="ignore") as fin:
		for line in fin:
			linesplit = line.strip().split()
			
			lexeme = linesplit[0][:-2]+"-"+linesplit[0][-1].lower()
		
			if to_load is None or lexeme in to_load:
				try:
					space[lexeme]= np.array([float(x) for x in linesplit[1:]])
				except:
					logger.warning("problem with line {}".format(line.strip()))	
	
	logger.info("Done loading dense space from file {}".format(filename))

	return space

def load_sparse (filename, to_load):
	'''
	  Load a sparse semantic space from a file.
	  
	  :param: filename the file from which the space has to be loaded. Must be a valid gzip file
	  :param: to_load set. Only the target in to_load will be included in the space. If it is None, all the targets are loaded
	  
	  :returns: SparseSpace instance representing the space
	'''

	rows = []
	cols = []
	data = []

	with gzip.open(filename, "rt", errors="ignore") as fin:
		for line in fin:
			w1, w2, f = line.strip().split()
			
			w1 = w1[:-2]+"-"+w1[-1].lower()
			w2 = w2[:-2]+"-"+w2[-1].lower()
			
			if to_load is None or w1 in to_load:
				rows.append(w1)
				cols.append(w2)
				data.append(float(f))
			
	space = SparseSpace(rows, cols, data)	
	
	logger.info("Done loading sparse space from file {}".format(filename))
	return space
	
	
def load_indexed (filename, to_load):
	'''
	  Load an indexed semantic space from a file.
	  
	  :param: filename the file from which the space has to be loaded
	  :param: to_load set. Only the target in to_load will be included in the space. If it is None, all the targets are loaded
	  
	  :returns: IndexedeSpace instance representing the space
	'''
	
	space = IndexedSpace(filename, to_load)
	
	return space

def load(filename, form = "dense", to_load = None):
	'''
	  Load a semantic space from a file.
	  
	  :param: filename the file from which the space has to be loaded
	  :param: form string (can be "dense", "sparse" or "indexed"). The kind of space to load. 
	  :param: to_load set. Only vectors for target elements in to_load will be included in the space. If it is None, all the targets are loaded
	  
	  :returns: an object representing the space
	'''
	
	if not form in ["dense", "sparse"]:
		fillerstype = "dense"
	
	fundict = {
		"dense": load_dense,
		"sparse": load_sparse,
		"indexed": load_indexed
	}
	
	space = fundict[form](filename, to_load)
	
	return space
