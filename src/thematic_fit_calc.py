'''
  SemanticSpace.py - main file, contains the main() function to execute the whole pipeline:
   · read configuration file
   · load dataset(s)
   · load fillers to test
   · compute scores for each filler
   · compute spearman's rank between the true scores and the predicted scores

  usage:
  python3 thematic_fit_calc.py {config file}
  
  for the config file format, see README.md

  
  Copyright (C) 2018  Enrico Santus, Ludovica Pannitto, Emmanuele Chersoni
  Ludovica.pannitto@unitn.it
    
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

  
'''

import sys
import os
import glob
import logging
import datetime

import scipy.spatial.distance
from scipy import stats

import ConfigReader
import Datasets
import SemanticSpace
import Fillers
import Prototypes

def usage ():
	sys.stderr.write( "usage:\n" )
	sys.stderr.write( "python3 thematic_fit_calc.py {config file}\n\n" )
	sys.stderr.write( "for more information see README.md\n" )

def main ():
	'''
	  read configuration file
      load dataset(s)
      load fillers to test
      compute scores for each filler
      compute spearman's rank between the true scores and the predicted scores
	'''
	
	if len(sys.argv) < 2:
		usage()
		exit(-1)
		
	#read configuration file for parameters
	cnf_filename = sys.argv[1]
	cnf = ConfigReader.ConfigMap(cnf_filename).parse()
	
	#create output and log dirs if they don't exist
	os.makedirs (cnf["outputdir"], exist_ok=True)
	os.makedirs (cnf["logdir"], exist_ok=True)
	
	#logging
	# create logger and file handler
	logger = logging.getLogger('thematic_fit_calc')
	logger.setLevel(logging.INFO)
	logger.propagate = False
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	
	fh = logging.FileHandler(cnf["logdir"]+'thematic_fit_calc-{}-{}.log'.format(cnf["name"], datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S")), mode="w")
	fh.setLevel(logging.INFO)
	fh.setFormatter(formatter)
	
	sh = logging.StreamHandler()
	sh.setLevel(logging.WARNING)
	sh.setFormatter(formatter)
	
	logger.addHandler(fh)
	logger.addHandler(sh)

	logger.info("loaded parameters, recap: \n\t{}".format("\n\t".join("{}: {}".format(par, cnf[par]) for par in cnf)))

	#output file
	fout =  open (cnf["outputdir"]+"{}-{}".format(cnf["name"], datetime.datetime.now().strftime("%Y_%m_%d_%H%M%S")), "w")
	
	needed_rows = set()
	dsmformat = cnf["dsmformat"]

	#load datasets
	dataset_name = cnf["dataset"]
	if dataset_name =="all":
		datasets = Datasets.load(glob.glob(cnf["datasetsdir"]+"*"), needed_rows)
		logger.info("loaded datasets from: {}".format(glob.glob(cnf["datasetsdir"]+"*")))
	else:
		datasets = Datasets.load(glob.glob(cnf["datasetsdir"]+dataset_name), needed_rows)
		logger.info("loaded datasets from: {}".format(glob.glob(cnf["datasetsdir"]+dataset_name)))

	#load list of fillers to test
	fillerspath = cnf["fillerspath"]
	fillersformat = cnf["fillersformat"]


	#just adding needed rows here
	for fillersname in glob.glob(fillerspath):
		fillers = Fillers.load(fillersname, datasets, needed_rows, fillersformat)

	logging.info("DSMS files: {}".format (glob.glob(cnf["dsmdir"]+cnf["dsm"])))

	#load semantic space
	for dsmname in glob.glob(cnf["dsmdir"]+cnf["dsm"]):

		logger.info("loading {}".format(dsmname))

		DSM = SemanticSpace.load(dsmname, form = dsmformat, to_load = needed_rows)
				
		fout.write ("DSM FILE: {}\n".format(dsmname))
		
		
		for fillersname in glob.glob(fillerspath):
			
			#load fillers
			fillers = Fillers.load(fillersname, datasets, needed_rows, fillersformat)
			
			fout.write ("FILLERS FILE: {}\n".format(fillersname))

			for i in range(cnf["min_fillersno"], cnf["max_fillersno"], cnf["step_fillersno"]):
				fout.write ("\tFILLERS NUMBER: {}\n".format(i))
				
				logger.info("building prototypes...")
				prototypes = Prototypes.create_proto(fillers, DSM, i)
				
				for d in datasets:
										
					fout.write ("\tDATASET: {}\n".format(d))
					
					logger.info("examining {} with {} fillers".format(d, i))
					skipped = 0
					
					true_scores = []
					predict_scores = []
					
					for target in datasets[d]:
						for rel, filler, score in datasets[d][target]:
							
							
							try:
								filler_v = DSM[filler]
								
								cosine = 1-scipy.spatial.distance.cosine(filler_v, prototypes[target][rel])
								
								
								true_scores.append((target, filler, rel, score))
								predict_scores.append((target, filler, rel, cosine))								
							except:
								logger.warning("filler {} not in model".format(filler))
								skipped += 1
					
					fout.write("\t\tskipped items: {}\n".format(skipped))
					fout.write("\t\tresults: {}\n".format(stats.spearmanr([x[3] for x in true_scores], [x[3] for x in predict_scores])))
				
				fout.write ("\n")
		
if __name__ == "__main__":
	main ()
