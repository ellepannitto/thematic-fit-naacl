'''

  ConfigReader.py - utility to parse config files
  
  Copyright (C) 2018  Enrico Santus, Ludovica Pannitto, Emmanuele Chersoni
  Ludovica.pannitto@unitn.it
    
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

'''

import os
from configparser import ConfigParser, ExtendedInterpolation

class ConfigMap:
	"""
	The class reads a configuration from file.
	It returns a map where indexes are lowercased strings from config file, and values are properly parsed from config file.
	"""
	
	def __init__ (self, cnf_file):
		"""
		Initializes a map to store parameters.
		
		Parameters:
		-----------
		cnf_file: string
			path of the file to open and read
			The file should be a standard configuration file accepted by ConfigParser class. It consists of sections, led by a [section] header (ignored by this class) and followed by name: value entries (name= value) is also accepted.
			Read more at https://docs.python.org/2/library/configparser.html
			
		At the moment, some values are casted to specific types or transformed, according to the following schema:
			min_fillersno -> int,
			max_fillersno -> int,
			step_fillersno -> int,
						
	    a sample config file can be found in conf/sample.cnf
	    For an exaustive description of the parameters, see README.md
		
		"""
		
		self.parser = ConfigParser(interpolation=ExtendedInterpolation())
		self.parser.read_file(open(cnf_file))
		
		self.parse_value = {
			"min_fillersno": int,
			"max_fillersno": int,
			"step_fillersno": int,
		}
		
		
	
	def parse (self):
		"""
		Returns:
		--------
		dict
			a map of the parameters specified. 
			The dictionary is in the form:
				cnf parameter name (lowercased) -> parsed value specified in cnf file 
		
		"""
		
		d = {}
		
		for section in self.parser.sections():
			for option in self.parser.options(section):
				d[option] = self.parser.get(section, option)		
				if option in self.parse_value:
					d[option] = self.parse_value[option](d[option])		
		
		self.values = d
		return d

if __name__ == "__main__":
	

	config = ConfigMap("../confs/prova.cnf")

	parameters = config.parse()	
	print(parameters)
