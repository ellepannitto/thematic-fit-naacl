'''
 
 Fillers.py - Contains functions that load fillers.
 Some sample filler files can be found in the fillers_samples/ directory
   
  Copyright (C) 2018  Enrico Santus, Ludovica Pannitto, Emmanuele Chersoni
  Ludovica.pannitto@unitn.it
    
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

'''


import collections

def load_dependencies(fname, to_load_dict, needed_rows):
	'''
	  Load fillers in the dependencies format.
	  
	  :param: fname the file from which the fillers have to be loaded
	  :param: to_load_dict dictionary containing targets and realtions for which fillers are required
	  :param: needed_rows set to store needed words that will be loaded from semantic space
	  
	  :returns: a dictionary containing, for each target in the datasets, for each syntactic relation, its top fillers.
	 
	 The input file should contain, on each line, the target word, a syntactic label in line with those of the datasets, a filler and their association score, as in the following example:
		
		advise-v sbj director-n 141.4141
		advise-v sbj service-n 141.2552
		advise-v obj client-n 1411.5521
		advise-v obj government-n 692.2554
		advise-v obj member-n 691.736
		advise-v obj student-n 678.3795	 

	'''

	print(to_load_dict)

	d = collections.defaultdict(lambda: collections.defaultdict(list))
	with open(fname) as fin:
		for line in fin:
			target, rel, filler, score = line.strip().split()
			if target in to_load_dict and rel in to_load_dict[target]:
				d[target][rel].append((filler, float(score)))
				
				needed_rows.add(filler)
				
	for target in d:
		for rel in d[target]:
			d[target][rel] = sorted(d[target][rel], key = lambda x: x[1], reverse = True)
			
	return d
	
def load_plain(fname, to_load_dict, needed_rows):
	'''
	  Load fillers in the plain format.
	  
	  :param: fname the file from which the fillers have to be loaded
	  :param: to_load_dict dictionary containing targets and realtions for which fillers are required
	  :param: needed_rows set to store needed words that will be loaded from semantic space
	  
	  :returns: a dictionary containing, for each target in the datasets, its top fillers.
	
	 The input file should contain, on each line, the target word, a filler and their association score, as in the following example:
		
		crush-v cane-n 1309.1637438593705
		crush-v tank-n 1293.2346720176752
		crush-v truck-n 1278.9576614391194
		crush-v powder-n 1258.737514156956
		crush-v wall-n 1251.8576024713607
		frighten-v people-n 4506.529251893827
		frighten-v child-n 2490.799204977875
		frighten-v horse-n 1602.341678594072
		frighten-v death-n 1582.7433623824356  
	  
	'''
	
	d = collections.defaultdict(lambda: collections.defaultdict(list))
	with open(fname) as fin:
		for line in fin:
			target, filler, score = line.strip().split()
			if target in to_load_dict:
				#~ d[target][rel].append((filler, float(score)))
				d[target]["all"].append((filler, float(score)))
				
				needed_rows.add(filler)
				
	for target in d:
		for rel in d[target]:
			d[target][rel] = sorted(d[target][rel], key = lambda x: x[1], reverse = True)
			
	return d

def load(fname, datasets_dict, needed_rows, fillerstype = "plain"):
	'''
	  Load fillers from a file.
	  
	  :param: fname the file from which the fillers have to be loaded
	  :param: datasets_dict dictionary representing datasets, therefore containing targets and realtions for which fillers are required
	  :param: needed_rows set to store needed words that will be loaded from semantic space
	  :param: fillerstype string (can be "plain",or "dependencies"), depending on the format of the files where fillers are stored.
	  
	  :returns: a dictionary containing, for each target in the datasets, for each syntactic relation, its top fillers.
	'''
	
	if not fillerstype in ["dependencies", "plain"]:
		fillerstype = "plain"
	
	fundict = {
		"dependencies": load_dependencies,
		"plain": load_plain
	}
	
	to_load = collections.defaultdict(set)
	for dataset in datasets_dict:
		for target in datasets_dict[dataset]:
			for rel, filler, score in datasets_dict[dataset][target]:
				to_load[target].add(rel)

	f = fundict[fillerstype](fname, to_load, needed_rows)
	

	return f
