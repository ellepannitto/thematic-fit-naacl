'''
 
 Datasets.py - helper functions to load various kind of datasets.
 Some sample datasets can be found in the data_samples directory. 
  
  Copyright (C) 2018  Enrico Santus, Ludovica Pannitto, Emmanuele Chersoni
  Ludovica.pannitto@unitn.it
    
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
  
 
'''


import collections
import logging
import os

logger = logging.getLogger('thematic_fit_calc')

def load_mcrae(fname, to_load):
	'''
	 Loads the mcrae dataset and adds all the loaded fillers and targets to the parameter to_load 
	 
	 a sample of the mcrae dataset can be found in data_samples/mcrae2.txt 
	 
	 :param: fname name of the file from which the dataset has to be loaded
	 :param: to_load set to which the loaded fillers and targets are added.	 
	 
	 :returns: a dictionary d as described in the documentation for the load function.
	 
	'''
	d = collections.defaultdict(list)
	
	with open(fname) as fin:
		for line in fin:
			
			target, filler, s1, s2 = line.strip().split()
			d[target].append(("sbj", filler, float(s1)))
			d[target].append(("obj", filler, float(s2)))
			
			to_load.add(target)
			to_load.add(filler)			
	
	logger.info ("loaded mcrae dataset from {}".format(fname))
	
	return d

def load_pado(fname, to_load):
	'''
	 Loads the pado dataset and adds all the loaded fillers and targets to the parameter to_load 
	 
	 a sample of the pado dataset can be found in data_samples/pado.txt 
	 
	 :param: fname name of the file from which the dataset has to be loaded
	 :param: to_load set to which the loaded fillers and targets are added.	 
	 
	 :returns: a dictionary d as described in the documentation for the load function.
	 
	'''
	
	d = collections.defaultdict(list)
	with open(fname) as fin:
		for line in fin:
			target, filler, rel, score = line.strip().split()
			d[target].append((rel, filler, float(score)))
			
			to_load.add(target)
			to_load.add(filler)
	
	logger.info ("loaded pado dataset from {}".format(fname))
	
	return d
	
def load_ferretti_instruments (fname, to_load):
	'''
	 Loads the Ferretti Instruments datasets and adds all the loaded fillers and targets to the parameter to_load 
	 
	 a sample of the Ferretti Instruments dataset can be found in data_samples/ferretti_dataset_instruments.txt 
	 
	 :param: fname name of the file from which the dataset has to be loaded
	 :param: to_load set to which the loaded fillers and targets are added.	 
	 
	 :returns: a dictionary d as described in the documentation for the load function.
	 
	'''
	d = collections.defaultdict(list)
	with open(fname) as fin:
		for line in fin:
			target, filler, s1 = line.strip().split()
			d[target].append(("with", filler, float(s1)))
			
			to_load.add(target)
			to_load.add(filler)
	
	logger.info ("loaded Ferretti Instruments dataset from {}".format(fname))
	
	return d

def load_ferretti_locations (fname, to_load):
	'''
	 Loads the Ferretti Locations datasets and adds all the loaded fillers and targets to the parameter to_load 
	 
	 a sample of the Ferretti Locations dataset can be found in data_samples/ferretti_dataset_locations.txt 
	 
	 :param: fname name of the file from which the dataset has to be loaded
	 :param: to_load set to which the loaded fillers and targets are added.	 

	 :returns: a dictionary d as described in the documentation for the load function.
	 
	'''
	d = collections.defaultdict(list)
	with open(fname) as fin:
		for line in fin:
			target, filler, s1 = line.strip().split()
			d[target].append(("loc", filler, float(s1)))
			
			to_load.add(target)
			to_load.add(filler)
	
	logger.info ("loaded Ferretti Locations dataset from {}".format(fname))
	
	return d
	
	
fundict = { 
		"mcrae2.txt": load_mcrae,
		"pado.txt": load_pado,
		"fakedataset.txt": load_pado,
		"ferretti_dataset_instruments.txt": load_ferretti_instruments,
		"ferretti_dataset_locations.txt": load_ferretti_locations,
}

def load(fileslist, to_load):
	'''
	 Loads multiple datasets from multiple files.
	 For each dataset, a dictionary d = { T:[( R1, F1, W1), ( R2, F2, W2), ... ] } is created
	 where T is a target and the elements of each tuple are:
	  R: relation
	  F: filler
	  W: weigth (score)
	  
	 The datasets type and format are infered from the file names. Nonexisting files are ignored and a WARNING is produced in the log.
	 Some sample datasets can be found in the data_samples directory. 

	 Also, this function adds all the loaded fillers and targets from all the datasets to the parameter to_load
	 
	 :param: fileslist list of the names of the file from which the datasets have to be loaded
	 :param: to_load set to which the loaded fillers and targets are added.
	 
	 :returns: a dictionary D with structure { fn: d } where each key fn is a dataset filename and its corresponding value d is a dictionary with the structure described above.
	 
	'''
	
	d = {}
	for filename in fileslist:
		if os.path.isfile (filename):
			filename_base = filename.split("/")[-1]
			d[filename_base] = fundict[filename_base](filename, to_load)
		else:
			logger.warning ( "file {} not found".format(filename) )
	return d
