**Thematic fit correlations scores**

---

## Requirements
This program requires python3 and depends on the python modules `numpy`, `scipy` and [igzip](https://pypi.org/project/indexed-gzip/). On most systems they can be easily installed using pip (with the right permissions):

```	
python3 -m pip install numpy scipy indexed-gzip
```

## How to run

```
python3 thematic_fit_calc.py {config file}
```

### Config file Format

The config file contains all the necessary parameters needed to run the tests, here you can find a complete list of the available options with a description attached:

```
[Name]
name: #name of the run, this will be included in the output and log file names

[Folders]
rootdir: #root directory of the project, typically ..
logdir: #directory to store the logs produced by the script, typically %(rootdir)s/log/
outputdir: #directory to store the output files produced by the script, typically %(rootdir)s/out/
datasetsdir: #directory where datasets are stored
dsmdir: #directory where the distributional models are stored
fillerspath: #path containing one ore more fillers file (this can be a glob pattern)

[Fillers]
fillersformat: #either dependencies or plain

[Datasets]
dataset: #filenames of wanted datasets, this can be a glob pattern (if set to 'all' or '*', all the datasets in the dataset folder are loaded)

[DSMs]
dsmformat: #either dense, sparse or indexed
dsm: #distributional model path. IMPORTANT: this needs to be explicit and in .gz format. It can be a glob pattern

[Parameters]
min_fillersno: #minimum number of fillers
max_fillersno: #maximum number of fillers
step_fillersno: #step width of range(min_fillersno, max_fillersno)
```

A sample config file can be found in `conf/sample.cnf`

### Dataset files Format

Multiple datasets format are supported: McRae, Pado, Ferretti Instruments and Ferretti Locations.
Some sample datasets files can be found in `data_samples/`.


### Fillers and DSMs file Format

A sample fillers file and DSMs file can be found in `fillers_samples` and in `dsms_samples` respectively.

#### Fillers

Filler files can either contain the dependency relation holding between the target and the filler (`dependencies` format) or not (`plain` format).
The latter file ((a sample can be found in `fillers_samples/fillers_plain.txt`) needs has the following format:
```
target	filler	score
```

while the latter (a sample can be found in `fillers_samples/fillers_plain.txt`) is similarly shaped as follows:
```
target	synrel	filler	score
```

Importantly, the `synrel` label has to match with the one loaded from the datasets.
We employed the following labels (please refer to the paper for a better overview):
- `sbj` for the subject relation (typically subsuming transitive and intransitive subjects)
- `obj` for the direct object relation
- `loc` for the location relation (typically subsuming prepositional modifiers introduced by `in`, `on`, `at`)
- `with` for the instrument relation (typically subsuming prepositional modifiers introduced by `with`)

At the moment, these parameters are hard-coded and cannot be modified through the config file.


#### DSMs file Format

The DSMs files need to be gzipped files, containing either:
- dense vectors (i.e., one vector per line, with the row label at the beginning of the line): this should be loaded with the `dense` option
- sparse vectors: the file should contain the sparse matrix, in a coordinate list fashion (i.e., each line is shaped as  `row_label column_label score`, as in the provided sample). Files of this kind can be loaded with the `sparse` option or the `indexed` option. Importantly, when using the latter option, the input file has to be sorted by row label and then by column label.

## License

This software is licensed under the GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
See `LICENSE.md` for more information.
Dependencies and datasets may be distributed under other licenses, please refer to the relative websites.

## Authors

* **Enrico Santus**
* **Ludovica Pannitto**
* **Emmanuele Chersoni**


